global exit

%define SYSWRITE 1
%define SYSREAD 0
%define SYSEXIT 60

%define STDERR 2
%define STDOUT 1
%define STDIN 0

%define TAB 0x9
%define NLINE 0xA
%define SPACE 0x20

section .text

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, SYSEXIT
	syscall

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 10
	call print_char
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.count:
		cmp byte[rdi + rax], 0	; если в строке не встретился нуль-терминатор,
		je .ret			; то начинаю цикл заново
		inc rax
		jmp .count
	.ret: ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi			; сохраняю значение регистра
	call string_length		; узнаю длину строки			
	pop rsi
	mov rdx, rax
	mov rdi, STDOUT
	mov rax, SYSWRITE
	syscall
	ret


; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rdx, 1
	mov rdi, STDOUT
	mov rax, SYSWRITE
	mov rsi, rsp
	syscall
	add rsp, 8
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push 0
	push 0
	push 0			;выделяю место в стеке под остатки
	mov rsi, 22
	mov rax, rdi
	mov r8, 10
	.div:
		xor rdx, rdx	;делю на 10, остаток сохраняю в стек
		div r8
		add rdx, "0"
		mov [rsp + rsi], dl
		dec rsi
		test rax, rax
		jnz .div
	lea rdi, [rsp + rsi + 1]
	call print_string
	add rsp, 24
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	cmp rdi, 0
	jns print_uint		;если число отрицательное, приписываю минус и меняю знак числа.
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	call print_uint
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push r12
	mov rax, -1
	.loop:
		inc rax
		mov r12b, byte[rdi + rax] 	;если встретился 0-терминатор, перехожу в .end
		test r12, r12
		jz .end
		cmp r12b, byte[rsi + rax]	;если символы равны, продолжаю проверять
		jz .loop
		jmp .not
	.end: 
		cmp r12b, byte[rsi + rax]	;если 0-терминаторы у обоих строк, то строки равны
		jnz .not
		mov rax, 1
		jmp .ret
		.not: mov rax, 0
		.ret: 
			pop r12
			ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
	mov rsi, rsp
	mov rdx, 1
	mov rax, SYSREAD
	mov rdi, STDIN
	syscall

	cmp rax, -1
	jne .char
	xor rax, rax
	add rsp, 8
	ret
	.char:
		pop rax
		ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13		; счетчик символов
	push r14		; начало буфера
	mov r13, 0
	mov r14, rdi
	mov r12, rsi
	.loop:
		call read_char		;пропускаю пробелы
		cmp rax, 0
		jz .end
		cmp rax, SPACE		
		jz .space
		cmp rax, TAB
		jz .space
		cmp rax, NLINE
		jz .space
			
		mov [r14 + r13], al
		inc r13
		cmp r13, r12
		jz .not
		jmp .loop
	.space:
		cmp r13, 0
		jz .loop
		jmp .end
	.not: 	mov rax, 0		;если строка больше размера буфера, то записываю в акк. 0
		jmp .ret
	.end: 	
		mov byte[r14 + r13], 0
		mov rax, r14
		mov rdx, r13
	.ret:	
		pop r14
		pop r13
		pop r12
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov rax, 0
	mov rsi, 0
	mov r9, 10
	.loop:
		mov r8b, byte[rdi + rsi]
		cmp r8, 0
		jz .number
		sub r8, "0"		;у каждого символа убираю символ "0", превращая символ в
		cmp r8, 10		;число
		jns .not_number
		cmp r8, -1
		js .not_number
		mul r9
		add rax, r8
		inc rsi
		jmp .loop
	.not_number:
		test rsi, rsi	;если первый символ не число, то спарсить невозможно, иначе имеют-
		jnz .number	;ся числа в начале строки
		mov rax, 0	;если в невозможно спарсить строку, записываю 0 в аккумулятор
		jmp .ret
	.number:
		mov rdx, rsi
	.ret:
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
	mov sil, byte[rdi]
	cmp rsi, '+'
	jnz .next
	inc rdi
	call parse_uint
	test rdx, rdx
	jz .ret
	inc rdx
	jmp .ret
.next:
	cmp rsi, '-'
	jnz parse_uint
	inc rdi
	call parse_uint
	test rdx, rdx
	jz .ret
	neg rax			;меняю знак, если в начале строки стоял минус
	inc rdx
.ret:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	mov rax, -1
	push r12
	.copy:
		inc rax
		cmp rax, rdx			;проверка на переполнение
		jz .overflow
		mov r12b, byte[rdi+rax]		;копирую по одному символу
		mov byte[rsi+rax], r12b
		test r12, r12
		jnz .copy
		jmp .ret
	.overflow:
		mov rax, 0
	.ret: 
		pop r12
		ret
